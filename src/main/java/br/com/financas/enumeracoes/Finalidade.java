package br.com.financas.enumeracoes;

public enum Finalidade{
  
  MORADIA("Moradia"),TRANSPORTE("Transporte"),EDUCACAO("Educação"),DIVERSOS("Diversos");
  
  private final String descricao;
  
  Finalidade(String descricao){
    this.descricao=descricao;
  }
  
  public String getDescricao(){
    return descricao;
  }
}
