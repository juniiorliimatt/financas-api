package br.com.financas.enumeracoes;

public enum TipoLancamento{
  
  RECEITA("receita"), DESPESA("despesa");
  
  private final String descricao;
  
  TipoLancamento(String descricao){
    this.descricao = descricao;
  }
  
  public String getDescricao(){
    return this.descricao;
  }
}
