package br.com.financas.enumeracoes;

public enum StatusLancamento{
  
  PENDENTE("pendente"),PAGO("pago");
  
  private final String descricao;
  
  StatusLancamento(String descricao){
    this.descricao=descricao;
  }
  
  public String getDescricao(){
    return this.descricao;
  }
}
