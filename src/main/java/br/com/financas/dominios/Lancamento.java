package br.com.financas.dominios;

import br.com.financas.controllers.dtos.LancamentoDTO;
import br.com.financas.enumeracoes.StatusLancamento;
import br.com.financas.enumeracoes.TipoLancamento;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name="lancamentos")
public class Lancamento implements Serializable{

  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;

  private String descricao;

  @Column(name="data_lancamento")
  @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
  private LocalDate dataLancamento;

  @Column(name="data_cadastro")
  @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
  private LocalDate dataCadastro;

  @Column(name="data_pagamento")
  @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
  private LocalDate dataPagamento;

  @Column(name="tipo_lancamento")
  @Enumerated(value=EnumType.STRING)
  private TipoLancamento tipoLancamento;

  @Column(name="status_lancamento")
  @Enumerated(value=EnumType.STRING)
  private StatusLancamento status;

  private BigDecimal valor;

  public Lancamento(){
  }

  public Lancamento(String descricao, LocalDate dataLancamento, LocalDate dataPagamento, TipoLancamento tipoLancamento,
      StatusLancamento status, BigDecimal valor){
    this.descricao=descricao;
    this.dataLancamento=dataLancamento;
    this.dataCadastro=LocalDate.now();
    this.dataPagamento=dataPagamento;
    this.tipoLancamento=tipoLancamento;
    this.status=status;
    this.valor=valor;
  }

  public Lancamento(Long id, String descricao, LocalDate dataLancamento, LocalDate dataPagamento,
      TipoLancamento tipoLancamento, StatusLancamento status, BigDecimal valor){
    this.id=id;
    this.descricao=descricao;
    this.dataLancamento=dataLancamento;
    this.dataCadastro=LocalDate.now();
    this.dataPagamento=dataPagamento;
    this.tipoLancamento=tipoLancamento;
    this.status=status;
    this.valor=valor;
  }

  public Lancamento(LancamentoDTO lancamento){
    this.id=lancamento.getId();
    this.descricao=lancamento.getDescricao();
    this.dataLancamento=lancamento.getDataLancamento();
    this.dataCadastro=LocalDate.now();
    this.dataPagamento=lancamento.getDataPagamento();
    this.tipoLancamento=lancamento.getTipoLancamento();
    this.status=lancamento.getStatus();
    this.valor=lancamento.getValor();
  }

  public Long getId(){
    return id;
  }

  public String getDescricao(){
    return descricao;
  }

  public LocalDate getDataLancamento(){
    return dataLancamento;
  }

  public LocalDate getDataCadastro(){
    return dataCadastro;
  }

  public LocalDate getDataPagamento(){
    return dataPagamento;
  }

  public TipoLancamento getTipoLancamento(){
    return tipoLancamento;
  }

  public StatusLancamento getStatus(){
    return status;
  }

  public BigDecimal getValor(){
    return valor;
  }

  public void setId(Long id){
    this.id=id;
  }

  public void setDescricao(String descricao){
    this.descricao=descricao;
  }

  public void setDataLancamento(LocalDate dataLancamento){
    this.dataLancamento=dataLancamento;
  }

  public void setDataCadastro(LocalDate dataCadastro){
    this.dataCadastro=dataCadastro;
  }

  public void setDataPagamento(LocalDate dataPagamento){
    this.dataPagamento=dataPagamento;
  }

  public void setTipoLancamento(TipoLancamento tipoLancamento){
    this.tipoLancamento=tipoLancamento;
  }

  public void setStatus(StatusLancamento status){
    this.status=status;
  }

  public void setValor(BigDecimal valor){
    this.valor=valor;
  }

  @Override
  public boolean equals(Object o){
    if(this==o){
      return true;
    }
    if(o==null || getClass()!=o.getClass()){
      return false;
    }
    Lancamento that=(Lancamento) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode(){
    return Objects.hash(id);
  }
}
