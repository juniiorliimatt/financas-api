package br.com.financas.services;

import br.com.financas.controllers.dtos.LancamentoDTO;
import br.com.financas.dominios.Lancamento;
import br.com.financas.repositories.LancamentoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class LancamentoService{

  private final LancamentoRepository repository;

  public LancamentoService(LancamentoRepository repository){
    this.repository=repository;
  }

  @Transactional(readOnly=true)
  public List<LancamentoDTO> buscarTodos(){
    return repository.findAll().stream().map(lancamento->new LancamentoDTO(lancamento)).collect(Collectors.toList());
  }

  @Transactional(readOnly=true)
  public Optional<Lancamento> buscarPorId(Long id){
    return repository.findById(id);
  }

  @Transactional
  public LancamentoDTO savarLancamento(LancamentoDTO lancamentoDTO){
    return new LancamentoDTO(repository.save(new Lancamento(lancamentoDTO)));
  }

  @Transactional
  public LancamentoDTO atualizarLancamento(LancamentoDTO lancamentoDTO, Optional<Lancamento> fromBD){
    Lancamento lancamentoAtualizado=fromBD.get();
    lancamentoAtualizado.setId(fromBD.get().getId());
    lancamentoAtualizado.setDescricao(lancamentoDTO.getDescricao());
    lancamentoAtualizado.setDataLancamento(lancamentoDTO.getDataLancamento());
    lancamentoAtualizado.setDataCadastro(lancamentoDTO.getDataCadastro());
    lancamentoAtualizado.setDataPagamento(lancamentoDTO.getDataPagamento());
    lancamentoAtualizado.setTipoLancamento(lancamentoDTO.getTipoLancamento());
    lancamentoAtualizado.setStatus(lancamentoDTO.getStatus());
    lancamentoAtualizado.setValor(lancamentoDTO.getValor());
    return new LancamentoDTO(repository.save(lancamentoAtualizado));
  }
}
