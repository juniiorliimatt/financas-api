package br.com.financas.controllers;

import br.com.financas.controllers.dtos.LancamentoDTO;
import br.com.financas.dominios.Lancamento;
import br.com.financas.services.LancamentoService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/lancamentos")
public class LancamentoController{

  private final LancamentoService service;

  public LancamentoController(LancamentoService service){
    this.service=service;
  }

  @PostMapping
  public ResponseEntity<LancamentoDTO> salvarLancamento(@RequestBody LancamentoDTO lancamentoDTO,
      UriComponentsBuilder builder){
    URI uri=builder.path("/{id}").buildAndExpand(lancamentoDTO.getId()).toUri();
    return ResponseEntity.created(uri).body(service.savarLancamento(lancamentoDTO));
  }

  @PutMapping("/{id}")
  public ResponseEntity<LancamentoDTO> atualizarLancamento(@PathVariable("id") Long id,
      @RequestBody LancamentoDTO lancamentoDTO){
    Optional<Lancamento> fromBD=service.buscarPorId(id);
    if(fromBD.isPresent()){
      return ResponseEntity.ok().body(service.atualizarLancamento(lancamentoDTO, fromBD));
    }
    return ResponseEntity.notFound().build();
  }

  @GetMapping
  public ResponseEntity<List<LancamentoDTO>> buscarTodos(){
    List<LancamentoDTO> lancamentos=service.buscarTodos();
    if(lancamentos.isEmpty()){
      return ResponseEntity.noContent().build();
    }
    return ResponseEntity.ok().body(lancamentos);
  }

  @GetMapping("/{id}")
  public ResponseEntity<LancamentoDTO> buscarPorId(@PathVariable Long id){
    Optional<Lancamento> lancamento=service.buscarPorId(id);
    if(lancamento.isEmpty()){
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok().body(new LancamentoDTO(lancamento.get()));
  }
}
