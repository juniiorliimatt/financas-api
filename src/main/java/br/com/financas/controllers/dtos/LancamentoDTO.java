package br.com.financas.controllers.dtos;

import br.com.financas.dominios.Lancamento;
import br.com.financas.enumeracoes.StatusLancamento;
import br.com.financas.enumeracoes.TipoLancamento;

import java.math.BigDecimal;
import java.time.LocalDate;

public class LancamentoDTO{

  private Long id;
  private String descricao;
  private LocalDate dataLancamento;
  private LocalDate dataCadastro;
  private LocalDate dataPagamento;
  private TipoLancamento tipoLancamento;
  private StatusLancamento status;
  private BigDecimal valor;

  public LancamentoDTO(){
  }

  public LancamentoDTO(Lancamento lancamento){
    this.id=lancamento.getId();
    this.descricao=lancamento.getDescricao();
    this.dataLancamento=lancamento.getDataLancamento();
    this.dataCadastro=LocalDate.now();
    this.dataPagamento=lancamento.getDataPagamento();
    this.tipoLancamento=lancamento.getTipoLancamento();
    this.status=lancamento.getStatus();
    this.valor=lancamento.getValor();
  }

  public Long getId(){
    return id;
  }

  public String getDescricao(){
    return descricao;
  }

  public LocalDate getDataLancamento(){
    return dataLancamento;
  }

  public LocalDate getDataCadastro(){
    return dataCadastro;
  }

  public LocalDate getDataPagamento(){
    return dataPagamento;
  }

  public TipoLancamento getTipoLancamento(){
    return tipoLancamento;
  }

  public StatusLancamento getStatus(){
    return status;
  }

  public BigDecimal getValor(){
    return valor;
  }
}
